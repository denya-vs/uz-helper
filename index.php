<meta charset="utf-8">
<?php
include_once('WebIcqLite.class.php');
echo '<pre>';

function send_icq($message = 'standart message', $to = '636723472') {
	$return = array('data' => '', 'error' => '');
	$icq = new WebIcqLite();
	if($icq->connect(633739284, '')){
			if(!$icq->send_message($to, $message)){
					$return['error'] = $icq->error;
			}else{
					$return['data'] ='Message sent';
			}
			$icq->disconnect();
	}else{
			$return['error'] = $icq->error;
	}
	return $return;
}

function write_log($message='standart text') {
	file_put_contents('logs.txt', date('Y-m-d H:i:s (T)'). '    '.$message.PHP_EOL, FILE_APPEND);
}

write_log('Start parsing');
//получаем токены
$response = exec($_SERVER['DOCUMENT_ROOT'].'/uz_parser/phantomjs parse.js');
// $response = exec($_SERVER['DOCUMENT_ROOT'].'/phantomjs parse.js');
if ($response == 'error') {
	write_log('Error parsing from phantom');
	exit;
}
$tokens = json_decode($response);

$url = "http://booking.uz.gov.ua/purchase/search/";

$data = array('another_ec' => '0',
 'date_dep' => '09.03.2015',
 'search'=>'',
 'station_from'=>'Котовськ',
 'station_id_from'=>'2208450',
 'station_id_till'=>'2208001',
 'station_till'=>'Одеса',
 'time_dep'=>'00:00',
 'time_dep_till'=>'',
 'train_search_form'=>''
 );

$headers = array('Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	'Accept-Encoding: gzip, deflate',
	'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
	'Cache-Control: no-cache',
	'Connection: keep-alive',
	'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
	'Cookie:'.$tokens->cookie,
	'GV-Ajax: 1',
	'GV-Referer: http://booking.uz.gov.ua/',
	'GV-Referer-Src: http://uz.gov.ua/',
	'GV-Referer-Src-Jump: 1',
	'GV-Screen: 1366x768',
	'GV-Token: '.$tokens->storage,
	'GV-Unique-Host: 1',
	'Host: booking.uz.gov.ua',
	'Pragma: no-cache',
	'Referer: http://booking.uz.gov.ua/',
	'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0'
	);

$ch = curl_init();
$curl_errors = '';
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
$json = curl_exec($ch);
$tickets = return_ticket($json);
if (!empty($tickets)) {
	$send_icq = send_icq("Есть {$tickets['places']} билетов на поезд {$tickets['number_train']}. Отправление в {$tickets['date_from']}");
	if (empty($send_icq['error'])) {
		write_log($send_icq['data']);
	} else {
		write_log($send_icq['error']);
	}
}
curl_close($ch);
echo 'done.';
echo '</pre>';

write_log('End parsing');

function return_ticket($json){
	$train_array = json_decode($json, TRUE);
	// print_r($train_array['value']);
	$info = $train_array['value'];
	foreach($info as $key=>$value){
		//возврат билета на определенный поезд на плацкарт
		/*
		if($value['num'] === '609Ш'){
			foreach ($value['types'] as $key2 => $value2) {
				if($value2['letter'] === 'П')
					return $value2['places'];
			}
		}
		*/
		//возврат билетов по заданному времени промежутку времени отправления
		if(strtotime($value['from']['src_date']) > strtotime('2015-03-09 08:00:00') && strtotime($value['from']['src_date']) < strtotime('2015-03-09 22:00:00')){
			foreach ($value['types'] as $key2 => $value2) {
				if($value2['letter'] === 'П')
					return array('number_train' =>$value['num'] , 'date_from' => $value['from']['src_date'], 'places' => $value2['places']);
			}
		}
	}
	return false;
}
?>